import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VolunteerComponent } from "./volunteer/volunteer.component";
import { EventAreaComponent } from "./event-area/event-area.component";
import { CausesComponent } from "./causes/causes.component";
import { FeaturedCausesComponent } from "./featured-causes/featured-causes.component";
import { TeamComponent } from "./team/team.component";
import { HomeBannerComponent } from "./home-banner/home-banner.component";
import { AboutusIndexComponent } from "./aboutus-index/aboutus-index.component";
import { BlogComponent } from "./blog/blog.component";
import { SubscribeComponent } from "./subscribe/subscribe.component";
import { HeaderComponent } from "./header/header.component";
import { AboutPageComponent } from "./about-page/about-page.component";
import { AboutBannerComponent } from "./about-banner/about-banner.component";


const routes: Routes = [
  { path: "", redirectTo: "/Home", pathMatch: "full" },
  { path: "Home", component: HeaderComponent },
  { path: "About", component: AboutPageComponent },
  { path: "Causes", component: CausesComponent },
  { path: "Events", component: EventAreaComponent },
  { path: "Blog", component: BlogComponent },
  { path: "Contact-Us", component: SubscribeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

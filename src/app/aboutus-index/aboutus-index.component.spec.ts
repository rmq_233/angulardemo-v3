import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutusIndexComponent } from './aboutus-index.component';

describe('AboutusIndexComponent', () => {
  let component: AboutusIndexComponent;
  let fixture: ComponentFixture<AboutusIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutusIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutusIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

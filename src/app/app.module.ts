import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VolunteerComponent } from './volunteer/volunteer.component';
import { EventAreaComponent } from './event-area/event-area.component';
import { CausesComponent } from './causes/causes.component';
import { FeaturedCausesComponent } from './featured-causes/featured-causes.component';
import { TeamComponent } from './team/team.component';
import { HomeBannerComponent } from './home-banner/home-banner.component';
import { AboutusIndexComponent } from './aboutus-index/aboutus-index.component';
import { BlogComponent } from './blog/blog.component';
import { FooterComponent } from './footer/footer.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { HeaderComponent } from './header/header.component';
import { AboutPageComponent } from './about-page/about-page.component';
import { AboutBannerComponent } from './about-banner/about-banner.component';
import { DefaultsComponent } from './layouts/defaults/defaults.component';

@NgModule({
  declarations: [
    AppComponent,
    VolunteerComponent,
    EventAreaComponent,
    CausesComponent,
    FeaturedCausesComponent,
    TeamComponent,
    HomeBannerComponent,
    AboutusIndexComponent,
    BlogComponent,
    FooterComponent,
    SubscribeComponent,
    HeaderComponent,
    AboutPageComponent,
    AboutBannerComponent,
    DefaultsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
